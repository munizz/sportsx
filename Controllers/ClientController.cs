using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json.Linq;
using SportsX.Data;
using SportsX.Models;

namespace SportsX.Controllers {
    
    [Route("clients")]
    public class ClientController : Controller
    {

        private readonly Context _context;
        
        public ClientController(Context context) {
            _context = context;
        }
        
        [HttpGet]
        public async Task<IActionResult>  GetAll() {
            var clients = _context.Clients.ToList();
            JArray data = clients.Count > 0 ? new JArray() : null;
            
            foreach (var client in clients) {
                
                var phones = from cliPhone in _context.ClientPhones
                    where cliPhone.ClientId == client.Id
                    select new {
                        phoneId = cliPhone.Id,
                        phoneNumber = cliPhone.Phone
                    };
                
                
                var company = from cliComp in _context.ClientCompanies
                    join comp in _context.Companies
                        on cliComp.CompanyId equals comp.Id
                    where cliComp.ClientId == client.Id
                    select new {
                        companyId = comp.Id,
                        companyName = comp.Name
                    };
                
                var clientType = from cliType in _context.ClientTypes
                    join t in _context.Types
                        on cliType.TypeId equals t.Id
                    where cliType.ClientId == client.Id
                    select new {
                        typeId = t.Id,
                        typeName = t.TypeName
                    };
                
                
                // Preparing data to append in JObject
                var phoneData = JArray.FromObject(phones);
                var companyData = JArray.FromObject(company);
                var clientTypeData = JArray.FromObject(clientType);

                // Building the final response body
                dynamic clientData = new JObject();
                clientData.clientId = client.Id;
                clientData.clientName = client.Name;
                clientData.clientEmail = client.Email;
                clientData.zipCode = client.ZipCode;
                clientData.points = client.Points;
                clientData.phones = phoneData;
                clientData.company = companyData;
                clientData.clientType = clientTypeData;

                data.Add(clientData);
            }
            
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(int id) {
            Client client = _context.Clients.FirstOrDefault(c => c.Id == id);

            if (client == null) return NotFound("There is no clients with given id =(");
            
            var phones = from cliPhone in _context.ClientPhones
                where cliPhone.ClientId == client.Id
                select new {
                    phoneId = cliPhone.Id,
                    phoneNumber = cliPhone.Phone
                };
                
                
            var company = from cliComp in _context.ClientCompanies
                join comp in _context.Companies
                    on cliComp.CompanyId equals comp.Id
                where cliComp.ClientId == client.Id
                select new {
                    companyId = comp.Id,
                    companyName = comp.Name
                };
                
            var clientType = from cliType in _context.ClientTypes
                join t in _context.Types
                    on cliType.TypeId equals t.Id
                where cliType.ClientId == client.Id
                select new {
                    typeId = t.Id,
                    typeName = t.TypeName
                };
                
                
            // Preparing data to append in JObject
            var phoneData = JArray.FromObject(phones);
            var companyData = JArray.FromObject(company);
            var clientTypeData = JArray.FromObject(clientType);

            // Building the final response body
            dynamic clientData = new JObject();
            clientData.clientId = client.Id;
            clientData.clientName = client.Name;
            clientData.clientEmail = client.Email;
            clientData.zipCode = client.ZipCode;
            clientData.points = client.Points;
            clientData.phones = phoneData;
            clientData.company = companyData;
            clientData.clientType = clientTypeData;

            return Ok(clientData);
        }

    }
}
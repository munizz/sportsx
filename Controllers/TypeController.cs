using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SportsX.Data;
using SportsX.Models;

namespace SportsX.Controllers {
    
    [Route("types")]
    public class TypeController : Controller {

        private readonly Context _context;
        
        public TypeController(Context context) {
            _context = context;
        }
        
        
        [HttpGet]
        public IEnumerable<Type> GetAll() {
            return _context.Types.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Type> GetById(int id) {
            return Ok(_context.Types.FirstOrDefault(t => t.Id == id));
        }
    }
}
using Microsoft.AspNetCore.Mvc;

namespace SportsX.Controllers {
    
    [Route("welcome")]
    public class WelcomeController : Controller {
        
        public ActionResult Index()
        {
            string salutationMessage = "Nome: Philippe Muniz Gomes";
            return Ok(salutationMessage);
        }
    }
}
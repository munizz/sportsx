using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using SportsX.Models;

namespace SportsX.Data {
    
    
    public class Context : DbContext {
        
        public Context(DbContextOptions<Context> options)
            : base(options)
        { }
        
        public DbSet<Client> Clients { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<ClientType> ClientTypes { get; set; }
        public DbSet<ClientPhone> ClientPhones { get; set; }
        public DbSet<ClientCompany> ClientCompanies { get; set; }
        
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>().ToTable("client");
            modelBuilder.Entity<Company>().ToTable("company");
            modelBuilder.Entity<Type>().ToTable("type");

            modelBuilder.Entity<ClientCompany>().ToTable("clientcompany")
                .HasKey( k => new {k.ClientId, k.CompanyId});

            modelBuilder.Entity<ClientType>().ToTable("clienttype")
                .HasKey(k => new { k.ClientId, k.TypeId});

            modelBuilder.Entity<ClientPhone>().ToTable("clientphone")
                .HasKey(k => new {k.Id});



        }
    }
    
}
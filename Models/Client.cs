using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsX.Models {
    public class Client {

        [Key] 
        private int _id;
        private string _name;
        private string _email;
        private string _zipCode;
        private int _points;
        
        [Column("id")]
        public int Id { get => _id; set => _id = value; }
        
        [StringLength(255)]
        [Column("name")]
        public string Name { get => _name; set => _name = value; }

        [StringLength(255)]
        [Column("email")]
        public string Email { get => _email; set => _email = value; }
        
        [StringLength(255)]
        [Column("zipcode")]
        public string ZipCode { get => _zipCode; set => _zipCode = value; }
        
        [Column("points")]
        public int Points { get => _points; set => _points = value; }
        
        
    }
}
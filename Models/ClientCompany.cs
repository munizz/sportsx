using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsX.Models {
    
    public class ClientCompany {
        
        private int _clientId;
        private int _companyId;
        
        
        [Key, Column("client_id")]
        [ForeignKey("client_id")]
        public int ClientId { get => _clientId; set => _clientId = value; }
        
        
        [StringLength(2)]
        [Key, Column("company_id")]
        [ForeignKey("company_id")]
        public int CompanyId { get => _companyId; set => _companyId = value; }        
    }
}
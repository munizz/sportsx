using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsX.Models {
    
    /**
     * Client Phones
     */
    public class ClientPhone {
        
        [Key]
        private int _id;

        private int _clientId;

        private string _phone;
        
        [Column("id")]
        public int Id { get => _id; set => _id = value; }

        [ForeignKey("client_id")]
        [Column("client_id")]
        public int ClientId { get => _clientId; set => _clientId = value; }
        
        [StringLength(15)]
        [Column("phone")]
        public string Phone { get => _phone; set => _phone = value; }        
    }
}
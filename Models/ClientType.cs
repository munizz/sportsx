using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsX.Models {
    public class ClientType {
        private int _clientId;
        private int _typeId;
        
        [Column("client_id")]
        [ForeignKey("client_id")]
        public int ClientId { get => _clientId; set => _clientId = value; }
        
        [StringLength(2)]
        [Column("type_id")]
        [ForeignKey("type_id")]
        public int TypeId { get => _typeId; set => _typeId = value; } 
    }
}
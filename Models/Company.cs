using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsX.Models{

    /**
     * Client Company 
     */
    public class Company {
        private int _id;
        private string _name;
        
        [Column("id")]
        public int Id { get => _id; set => _id = value; }
        
        [StringLength(100)]
        [Column("name")]
        public string Name { get => _name; set => _name = value; }
    }
}
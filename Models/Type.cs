using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsX.Models {
    
    /**
     * Client types (PF or PJ)
     */
    public class Type {
        private int _id;
        private string _type;
        
        [Column("id")]
        public int Id { get => _id; set => _id = value; }
        
        [StringLength(2)]
        [Column("typename")]
        public string TypeName { get => _type; set => _type = value; }
    }
}
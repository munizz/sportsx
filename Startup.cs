using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using SportsX.Data;

namespace SportsX
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddNewtonsoftJson();
            
            string connectionString = Configuration["DbContextSettings:ConnectionString"];
            services.AddDbContext<Context>(opts =>
            {
                opts.UseNpgsql(connectionString);
//                var loggerFactory = LoggerFactory.Create(builder => {
//                        builder.AddFilter("Microsoft", LogLevel.Warning)
//                            .AddFilter("System", LogLevel.Warning)
//                            .AddFilter("SampleApp.Program", LogLevel.Debug)
//                            .AddConsole();
//                    }
//                );
//                opts.UseLoggerFactory(loggerFactory);               
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting(routes => {
                routes.MapApplication();
            });

            app.UseAuthorization();
        }
    }
}
